/* autobracket.vala
 *
 * Copyright 2020 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

private class Autobracket : Object, Proton.PluginIface
{
    private weak Proton.PluginManager pm;
    private HashTable<string, string> keys;
    private HashTable<string, string> closing_keys;

    public void do_register(Proton.PluginManager pm)
    {
        this.pm = pm;
        this.init_table();
        this.pm.window.manager.created.connect(this.on_create_hook);
    }

    private void init_table()
    {
        this.keys = new HashTable<string, string>(str_hash, str_equal);
        this.keys.set("parenleft", "()");
        this.keys.set("braceleft", "{}");
        this.keys.set("bracketleft", "[]");
        this.keys.set("apostrophe", "''");
        this.keys.set("quotedbl", "\"\"");
        this.keys.set("grave", "``");
        // this.keys.set("less", "<>");

        // This is used for the skip-if-next behavior
        this.closing_keys = new HashTable<string, string>(str_hash, str_equal);
        this.closing_keys.set("parenright", ")");
        this.closing_keys.set("braceright", "}");
        this.closing_keys.set("bracketright", "]");
        this.closing_keys.set("apostrophe", "'");
        this.closing_keys.set("quotedbl", "\"");
        this.closing_keys.set("grave", "`");
    }

    private void on_create_hook(Proton.Editor ed)
    {
        if (ed == null)
            return ;
        ed.sview.key_press_event.connect((e) => {
            string left = Gdk.keyval_name(e.keyval);
            string aux;

            if (this.closing_keys.contains(left))
            {
                // Allow the next check to occur if we did not skip
                if (this.skip_if_next(ed, this.closing_keys.get(left)))
                    return (true);
            }
            if (this.keys.contains(left))
            {
                aux = this.keys.get(left);
                this.insert_bracket(ed, aux, aux.offset(1));
                return (true);
            }
            return (false);
        });
    }

    private void insert_bracket(Proton.Editor ed, string left, string right)
    {
        Gtk.TextIter start = {};
        Gtk.TextIter end = {};
        Gtk.TextMark mk;
        string text;

        ed.buffer.begin_user_action(); // ...

        if (ed.buffer.has_selection &&
            ed.buffer.get_selection_bounds(out start, out end))
        {
            text = ed.buffer.get_text(start, end, false);
            mk = ed.buffer.create_mark("fk_sel", start, false);

            ed.buffer.insert(ref start, left, 1);
            end = start;
            end.forward_chars(text.length);
            ed.buffer.insert(ref end, right, 1);

            ed.buffer.get_iter_at_mark(out start, mk);
            end = start;
            end.forward_chars(text.length);
            ed.buffer.select_range(start, end);
        }
        else
        {
            ed.buffer.get_iter_at_offset(out start, ed.buffer.cursor_position);
            ed.buffer.insert(ref start, left, 2);
            start.backward_char();
            ed.buffer.place_cursor(start);
        }
        ed.buffer.end_user_action(); // ...
    }

    // If the user types a closing bracket char and the char in front (after) of
    // the caret is that same character we intercept the text and only move the
    // caret forwards.

    private bool skip_if_next(Proton.Editor ed, string closing)
    {
        Gtk.TextIter it, end;
        string text;

        ed.buffer.get_iter_at_offset(out it, ed.buffer.cursor_position);
        end = it;
        end.forward_char();
        text = ed.buffer.get_text(it, end, false);
        if (text == closing)
        {
            ed.buffer.begin_user_action();
            ed.buffer.place_cursor(end);
            ed.buffer.end_user_action();
            return (true);
        }
        return (false);
    }

    public void activate()
    {
    }

    public void deactivate()
    {
    }
}

public Type register_plugin(Module module)
{
    return typeof(Autobracket);
}
