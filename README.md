# Autobracket plugin for [Proton](https://gitlab.com/raggesilver-proton/proton)

Automatically complete parenthesis on the fly.

## Features

- Can close: `(`, `{`, `[`, `'`, `"`, ``` ` ```
- Selected text will be wrapped upon insertion of any of the supported chars

> HTML '<' is disabled for now
